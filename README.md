# V4L-MAUVE

This package provides MAUVE components to use camera through Video4Linux (actually through the OpenCV API).

It is licensed under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0).

## Instal Instructions

You first need to have installed the MAUVE [Toolchain](https://gitlab.com/mauve/mauve_toolchain).

To install the V4L-MAUVE package in the MAUVE workspace:
```
cd ~/mauve_ws
git clone https://gitlab.com/MAUVE/v4l_mauve.git src/v4l_mauve
catkin_make
```

## Documentation

### Camera component

The Camera component has a PeriodicStateMachine, and provides as interface:

* an output port _image_ with the grabbed image
* an output port _time_ with the image timestamp (as reported by the camera)
* a property _id_ with the camera ID to connect to (-1 connects to the first camera found)
* a property _fps_ to set the FPS of the camera

### Testing

To test the connection with the camera:
```
rosrun v4l_mauve v4l_mauve_component
```
