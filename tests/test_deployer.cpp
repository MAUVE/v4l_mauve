/****
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE V4L project.
 *
 * MAUVE V4L is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE V4L is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 ****/
#include "v4l/mauve/Camera.hpp"

using namespace mauve::runtime;
using namespace v4l::mauve;
using namespace mauve::types::sensor;

struct CameraTestArchitecture : public Architecture {
  Camera & camera = mk_component< Camera >("camera");
  SharedData<Image> & img = mk_resource< SharedData<Image> >("image", Image());

  bool configure_hook() override {
    camera.shell().image.connect(img.interface().write);
    camera.shell().fps.set(1);
    camera.configure();
    return true;
  }
};

extern "C" void mk_python_deployer() {
  CameraTestArchitecture* archi = new CameraTestArchitecture();
  auto depl = mk_abstract_deployer(archi);
}
