#!/usr/bin/ipython -i
#####
# Copyright 2018 ONERA
#
# This file is part of the MAUVE V4L project.
#
# MAUVE V4L is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE V4L is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
import mauve_runtime
mauve_runtime.load_deployer("devel/lib/libmauve_v4l_deployer.so")

mauve_runtime.initialize_logger(b'''
default:
  level: debug
  type: stdout
''')

deployer = mauve_runtime.deployer
deployer.architecture.configure()
deployer.create_tasks()
deployer.activate()
deployer.start()
